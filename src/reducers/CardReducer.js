export const cardReducer = (state = (localStorage.getItem('cardItems')) ? JSON.parse(localStorage.getItem('cardItems')) : {}, action) => {
    let newState;
    switch (action.type) {
        case "ADD_ITEM":
            newState = {...state};
            if (newState[action.payload.userId]) {
                let userCard = newState[action.payload.userId];
                let item = userCard.find(item => item.id === action.payload.item.id)
                if (item) {
                    if (action.payload.cardCount + item.cardCount <= item.count) {
                        item.cardCount += action.payload.cardCount
                    }
                } else {
                    if (action.payload.cardCount <= action.payload.item.count) {
                        userCard.push({...action.payload.item, cardCount: action.payload.cardCount})
                    }
                }
            } else {
                if (action.payload.cardCount <= action.payload.item.count) {
                    newState[action.payload.userId] = [{...action.payload.item, cardCount: action.payload.cardCount}]
                }
            }
            return newState
        case "REMOVE_ITEM":
            newState = {...state};
            if (newState[action.payload.userId]) {
                newState[action.payload.userId] = newState[action.payload.userId].filter(item => {
                    return item.id !== action.payload.itemId
                });
            }
            ;
            return newState
        case "EMPTY_CARD":
            newState = {...state};
            if (!action.payload.card) {
                newState = {}
            }
            ;
            return newState;
        default:
            return state;
    }
}