export const authReducer = (state = {
    auth: !!localStorage.getItem('loggedUser'),
    loading: false,
    user: JSON.parse(localStorage.getItem('loggedUser')),
    error: ""
}, action) => {
    switch (action.type) {
        case "LOGIN_START":
            return {
                ...state,
                ...action.payload
            };
        case "LOGIN_SUCCESS":
            return {
                ...state,
                ...action.payload
            };
        case "LOGIN_FEILD":
            return {
                ...state,
                ...action.payload
            };
        default:
            return state;
    }
}