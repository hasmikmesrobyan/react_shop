export const addToCard = (item, chosenCount, dispatch, state, callback) => {
    if (item.count >= chosenCount) {
        const userId = JSON.parse(localStorage.getItem('loggedUser')).id;
        let items = localStorage.getItem('cardItems')
        if (items) {
            let userCard = JSON.parse(items)[userId],
                storedProduct = userCard.find(attr => attr.id === item.id)
            if (storedProduct && storedProduct.count >= storedProduct.cardCount + chosenCount) {
                dispatch({type: "ADD_ITEM", payload: {item, userId: state.user.id, cardCount: chosenCount}});
                storedProduct.cardCount += chosenCount;
                localStorage.setItem('cardItems', JSON.stringify({[userId]: userCard}));
            } else if (!storedProduct) {
                dispatch({type: "ADD_ITEM", payload: {item, userId: state.user.id, cardCount: chosenCount}});
                userCard.push({...item, cardCount: chosenCount})
                localStorage.setItem('cardItems', JSON.stringify({[userId]: userCard}));
            } else {
                callback(`There's not enough quantity of this product!`, 'error')
            }
        } else {
            dispatch({type: "ADD_ITEM", payload: {item, userId: state.user.id, cardCount: chosenCount}});
            let localBase = {};
            localBase[userId] = []
            localBase[userId].push({...item, cardCount: chosenCount})
            localStorage.setItem('cardItems', JSON.stringify(localBase))
        }
    } else {
        callback(`There's not enough quantity of this product!`, 'error')
    }
}