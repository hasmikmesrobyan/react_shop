import React from "react";
import styled from "styled-components";

const MainTitle = styled.h2`
    font-size: 24px;
    color: #f8a406;
    text-transform: uppercase;
    margin: 0 auto 40px;
    width: max-content;
    font-weight: 900;
    text-align: center;
    
    @media (max-width: 768px) {
        font-size: 22px;
    }
`

export default function Title({title}) {
    return <MainTitle>{title}</MainTitle>
}