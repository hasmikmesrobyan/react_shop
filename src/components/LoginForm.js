import React from "react";
import {useDispatch} from "react-redux";
import {useTranslation} from "react-i18next";
import {useFormik} from "formik";
import registeredUsers from '../data/users.json';
import close from '../img/icons/close.svg';
import * as Main from "../css/Main_styles";
import * as S from "./loginFrom_styles";

export default function LoginForm({onClose}) {

    const dispatch = useDispatch();

    const [t, i18n] = useTranslation('common');

    const validate = values => {
        const errors = {};
        if (!values.name) {
            errors.name = t('loginForm.required');
        }

        if (!values.password) {
            errors.password = t('loginForm.required');
        }

        return errors;
    };

    const formik = useFormik({
        initialValues: {
            name: '',
            password: ''
        },
        validate,
        isSubmitting: true,
        onSubmit: values => {
            dispatch({type: "LOGIN_START", payload: {loading: true}});
            const user = registeredUsers.users.find((item, i) => item.userName === values.name && item.password === values.password);
            if (user) {
                dispatch({
                    type: "LOGIN_SUCCESS",
                    payload: {auth: true, user: {id: user.id, userName: user.userName}, loading: false}
                })
                localStorage.setItem('loggedUser', JSON.stringify({id: user.id, userName: user.userName}));
                // errorMsg(`You are logged in successfully`, 'success')
            } else {
                dispatch({type: "LOGIN_FEILD", payload: {loading: false, error: "Wrong credentials."}})
                // errorMsg(`wrong credentials`, 'error')
            }
        }
    })


    return (
        <S.FormContainer className={"login opened"}>
            <S.Form onSubmit={formik.handleSubmit}>
                <S.CloseButton src={close}
                               alt="close"
                               onClick={onClose}/>
                <Main.Input type="text"
                            id={'name'}
                            placeholder={t('loginForm.userName')}
                            onChange={formik.handleChange}
                            value={formik.values.name}/>
                {formik.errors.name ? <p>{formik.errors.name}</p> : null}
                <Main.Input type="password"
                            id={'password'}
                            placeholder={t('loginForm.password')}
                            onChange={formik.handleChange}
                            value={formik.values.password}/>
                {formik.errors.password ? <p>{formik.errors.password}</p> : null}
                <Main.Input type="submit" value={t('loginForm.submit')}/>
            </S.Form>
        </S.FormContainer>
    );
}