import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from "react-i18next";
import Menu from '../components/Menu';
import LoginForm from '../components/LoginForm';
import * as Main from '../css/Main_styles';
import * as S from "./header_style";
import logo from '../img/logo.png';
import user from '../img/icons/user.svg';
import cardIcon from '../img/icons/customer.svg';

export default function Header() {

    const history = useHistory();

    const dispatch = useDispatch();

    const [t, i18n] = useTranslation('common');

    const [loginFormVisibility, setLoginFormVisibility] = useState(false);

    const [notificationMessage, setNotificationMessage] = useState({
        classname: '',
        messageVisibility: false,
        messageText: ''
    });

    useEffect(() => {
        let messageTimer = setTimeout(() => {
            setNotificationMessage((args) => ({
                ...args,
                messageVisibility: false
            }))
        }, 2000)
        return () => {
            clearTimeout(messageTimer);
        }
    }, [notificationMessage.messageVisibility])

    const openLogin = () => setLoginFormVisibility(true)

    const closeForm = () => setLoginFormVisibility(false)

    const errorMsg = (msg, classname) => {
        setNotificationMessage(() => ({
            classname: classname,
            messageVisibility: true,
            messageText: msg
        }))
    }

    const logout = (e) => {
        e.preventDefault();
        localStorage.removeItem('loggedUser')
        dispatch({type: "LOGIN_FEILD", payload: {auth: false}});
        errorMsg(`You're logged out`, 'error')
        history.push("/");
        localStorage.getItem('cardItems') && localStorage.removeItem('cardItems');
        dispatch({type: "EMPTY_CARD", payload: {card: false}});

    }

    const state = useSelector(state => {
        return {card: state.card, auth: state.auth}
    });

    let card = Object.keys(state.card).length > 0 && state.auth.auth ? state.card[state.auth.user.id] : [],
        totalAmount = 0;

    !!card.filter(item => {
        return totalAmount += parseInt(item.cardCount, 10);
    })


    return (
        <React.Fragment>
            <S.Header>
                <Main.Wrapper>
                    <S.Nav>
                        <S.LogoContainer to="/">
                            <S.LogoImage src={logo} alt="logo"/>
                        </S.LogoContainer>
                        <S.MenuContainer>
                            <ul>
                                <Menu/>
                            </ul>
                            {state.auth.auth &&
                            <S.CardLink to="/card"
                                        className={'card icon'}>
                                <S.CardLinkImage src={cardIcon}
                                                 alt="card"/>
                                <S.CardItemsCount
                                    className={totalAmount > 0 ? "active" : "passive"}>{totalAmount}</S.CardItemsCount>
                            </S.CardLink>
                            }
                            {state.auth.auth ?
                                <div>
                                    <S.Button onClick={logout}>{t('header.logout')}</S.Button>
                                    <span> / </span>
                                    <S.UserName>{(state.auth.user && state.auth.user != null) && state.auth.user.userName}</S.UserName>
                                </div>
                                :
                                <S.Button onClick={openLogin}
                                          className={'icon user'}>
                                    <S.UserIcon src={user}
                                                alt="login"/>
                                </S.Button>
                            }
                            <S.LangBtn onClick={() => i18n.changeLanguage('en')}>EN</S.LangBtn>
                            <S.LangBtn onClick={() => i18n.changeLanguage('arm')}>ARM</S.LangBtn>
                            <S.LangBtn onClick={() => i18n.changeLanguage('ru')}>RU</S.LangBtn>
                        </S.MenuContainer>
                    </S.Nav>
                </Main.Wrapper>
                {
                    loginFormVisibility &&
                    <LoginForm onClose={closeForm}/>
                }
                {/*{*/}
                {/*    notificationMessage.messageVisibility &&*/}
                {/*    <NotificationMsg message={notificationMessage.messageText} class={notificationMessage.classname}/>*/}
                {/*}*/}
            </S.Header>
        </React.Fragment>
    );
}