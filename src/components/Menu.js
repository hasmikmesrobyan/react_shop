import React from "react";
import {useTranslation} from "react-i18next";
import * as S from './Menu_style'

export default function Menu() {
    const [t, i18n] = useTranslation('common');

    return (
        <React.Fragment>
            <S.MenuList>
                <S.MenuItem to='/' replace={true}>{t('header.home')}</S.MenuItem>
                <S.MenuItem to='/test' replace={true}>test</S.MenuItem>
            </S.MenuList>
        </React.Fragment>
    )
}