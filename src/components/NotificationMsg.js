import React from "react";
import * as Main from "../css/Main_styles";
import * as S from "./Notification_styles";

export default function SuccessMsg({message, classname}) {
    return (
        <S.MessageContainer className={classname}>
            <Main.Wrapper>
                <p>{message}</p>
            </Main.Wrapper>
        </S.MessageContainer>
    );
}