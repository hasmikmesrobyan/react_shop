import React from "react"
import {Redirect, Switch, Route} from "react-router-dom";
import {useSelector} from 'react-redux';
import PubicRoutes from "./../Routes.js";
import PrivateRoutes from "./../PrivateRoutes.js";

const switchPublicRoutes = (
    <Switch>
        {PubicRoutes.map((prop, key) => {
            return (
                <Route
                    exact
                    path={prop.path}
                    component={prop.component}
                    key={key}
                />
            );
        })}
    </Switch>
);

const switchPrivateRoutes = (
    <Switch>
        {PrivateRoutes.map((prop, key) => {
            return (
                <Route
                    exact
                    path={prop.path}
                    component={prop.component}
                    key={key}
                />
            );
        })}
    </Switch>
);

export default function Main() {
    const state = useSelector(state => state.auth);
    return <React.Fragment>
        {state.auth ? switchPrivateRoutes : switchPublicRoutes}
        <Redirect to="/"/>
    </React.Fragment>
}