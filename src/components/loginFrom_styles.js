import styled from "styled-components";

export const FormContainer = styled.div`
    position: absolute;
    top: 90px;
    left: 0;
    right: 0;
    margin: 0 auto;
    background-color: #e1dee2;
    padding: 20px;
    width: 500px;
    border-radius: 10px;
    padding-top: 40px;
    transform: translateY(-200%);
    transition: 0.5s all linear;
    
    &.opened{
        transform: translateY(0);
    }
    
    @media (max-width: 600px) {
        width: 90%;
        margin: 0 auto;
    }
`
export const Form = styled.form`
    display: flex;
    flex-direction: column;
    align-items: center;
`
export const CloseButton = styled.img`
    height: 15px;
    position: absolute;
    right: 10px;
    top: 10px;
    cursor: pointer;
`