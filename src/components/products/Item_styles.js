import styled from "styled-components";
import {Link} from "react-router-dom";


export const ReadMore = styled(Link)`
    margin-top: 20px;
    color: #290034;
    position: relative;
    padding: 5px;
    border: none;
    background-color: transparent;
    height: auto;
    width: auto;
    display: inline-flex;
    
    &:after{
        content: "";
        position: absolute;
        bottom: -3px;
        left: 0;
        right: 0;
        width: 0;
        transition: 0.3s all linear;
        background-color: #290034;
        height: 2px;
    }
    
    &:hover:after{
        width: 100%;
    }
`