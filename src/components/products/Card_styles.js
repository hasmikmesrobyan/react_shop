import styled from "styled-components";
import {Link} from "react-router-dom";


export const CardContainer = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    
    @media (max-width: 600px) {
        width: 70%;
        margin: 0 auto;
    }
    
    @media (max-width: 480px) {
        width: 100%;
    }
`
export const LinkToSingle = styled(Link)`
    width: 30%;
    height: 250px;
`
export const TotalAmount = styled.p`
    text-align: right;
    font-size: 1rem;
    color: #f8a406;
    margin: 0 0 20px auto;
`
export const TotalAmountValue = styled.span`
    font-weight: bolder;
    font-size: 18px;
`