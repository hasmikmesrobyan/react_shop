import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from "react-i18next";
import NotificationMsg from '../NotificationMsg';
import {addToCard} from "../CustomHook";
import * as Main from "../../css/Main_styles";

export default function SingleProduct({item}) {

    const dispatch = useDispatch();

    const [t, i18n] = useTranslation('common');

    const state = useSelector(state => state.auth);

    const [notificationMessage, setNotificationMessage] = useState({
        classname: '',
        messageVisibility: false,
        messageText: ''
    });

    const [chosenCount, setChosenCount] = useState(1)

    useEffect(() => {
        let messageTimer = setTimeout(() => {
            setNotificationMessage((args) => ({
                ...args,
                messageVisibility: false
            }))
        }, 2000)
        return () => {
            clearTimeout(messageTimer);
        }
    }, [notificationMessage.messageVisibility])

    const errorMsg = (msg, classname) => {
        setNotificationMessage(() => ({
            classname: classname,
            messageVisibility: true,
            messageText: msg
        }))
    }

    const onCount = (e) => {
        setChosenCount(parseInt(e.target.value, 10));
        if (parseInt(e.target.value, 10) > parseInt(e.target.max, 10)) {
            setChosenCount(parseInt(e.target.max, 10))
            errorMsg(`Value can't be more than in stock`, 'error');
        } else if (parseInt(e.target.value, 10) < parseInt(e.target.min, 10)) {
            setChosenCount(parseInt(e.target.min, 10))
            errorMsg(`Value can't be less than 1 item`, 'error');
        }
    }

    const submit = (item) => {
        addToCard(item, chosenCount, dispatch, state, errorMsg)
    }

    const {description, img, title, count, price} = item;

    let convertEdPrice = parseInt(price.split('$')[0], 10) + ' $';

    const [priceValue, setPriceValue] = useState(convertEdPrice)

  /*  useEffect(() => {
        if(i18n.languages[0] === "arm"){
            setPriceValue(parseInt(price.split('$')[0], 10) * 480 + ' AMD')
        }else if(i18n.languages[0] === "ru"){
            setPriceValue(parseInt(price.split('$')[0], 10) * 7 + ' RUB')
        }else{
            setPriceValue(convertEdPrice)
        }
    },[i18n])*/

    return (
        <React.Fragment>
            <Main.SingleItem className={'single_product'}>
                <Main.ItemImage className={'single_product'} style={{backgroundImage: `url(${img})`}}></Main.ItemImage>
                <Main.DescriptionContainer className={'single_product'}>
                    <Main.Title className={'single_product'}>{title}</Main.Title>
                    <Main.DescriptionText>{description}</Main.DescriptionText>
                    <Main.ItemPrice className={'price'}>{t('singleItem.price')}: {priceValue}</Main.ItemPrice>
                    {state.auth &&
                    <React.Fragment>
                        <Main.Label>
                            {t('singleItem.quantity')}
                            <Main.Input className={'single_product'}
                                        type="number"
                                        min="1"
                                        value={chosenCount}
                                        max={count}
                                        onChange={onCount}/>
                            <Main.InStock>{t('common.inStock')} {count}</Main.InStock>
                        </Main.Label>
                        <Main.Button className={'single_product'}
                                     onClick={() => submit(item)}>
                            {t('common.addToCard')}
                        </Main.Button>
                    </React.Fragment>
                    }
                </Main.DescriptionContainer>
            </Main.SingleItem>
            {
                notificationMessage.messageVisibility &&
                <NotificationMsg message={notificationMessage.messageText} classname={notificationMessage.classname}/>
            }
        </React.Fragment>
    );
}