import React, {useState, useEffect}  from "react"
import { useSelector, useDispatch } from 'react-redux';
import {useTranslation} from "react-i18next";
import NotificationMsg from '../NotificationMsg';
import { addToCard } from "../CustomHook";
import * as S from "./Item_styles";
import * as Main from "../../css/Main_styles";

export default function Item({data}) {
    const dispatch = useDispatch();

    const [t, i18n] = useTranslation('common');

    const [notificationMessage, setNotificationMessage] = useState({
        classname: '',
        messageVisibility: false,
        messageText: ''
    });
    
    const [chosenCount, setChosenCount] = useState(1);

    const state = useSelector(state => state.auth);

    useEffect(()=>{
        let messageTimer =  setTimeout(()=>{
            setNotificationMessage((args) => ({
                ...args,
                messageVisibility: false
            }))
        },2000)
        return () => {
            clearTimeout(messageTimer);
        }
    },[notificationMessage.messageVisibility])

    const errorMsg = (msg, classname) => {
        setNotificationMessage(() => ({
            classname: classname,
            messageVisibility: true,
            messageText: msg
        }))
    }

    const onChange = (e) => {
        setChosenCount(parseInt(e.target.value, 10))
        if (parseInt(e.target.value, 10) > parseInt(e.target.max, 10)) {
            setChosenCount(parseInt(e.target.max, 10))
            errorMsg(`Value can't be more than in stock`,'error');
        } else if (parseInt(e.target.value, 10) < parseInt(e.target.min, 10)) {
            setChosenCount(parseInt(e.target.min, 10))
            errorMsg(`Value can't be less than 1`,'error');
        }
    }

    const onBuy = (item) => {
        addToCard(item, chosenCount, dispatch, state, errorMsg)
    }


    const {img, title, id, count, price} = data;

    let convertEdPrice = parseInt(price.split('$')[0], 10) + ' $';

    const [value, setValue] = useState(convertEdPrice)

  /*  useEffect(() => {
        if(i18n.languages[0] === "arm"){
            setValue(parseInt(price.split('$')[0], 10) * 480 + ' AMD')
        }else if(i18n.languages[0] === "ru"){
            setValue(parseInt(price.split('$')[0], 10) * 7 + ' RUB')
        }else{
            setValue(convertEdPrice)
        }
    })*/

    return (
        <React.Fragment>
            <Main.SingleItem>
                <Main.ItemImage style={{backgroundImage: `url(${img})`}}></Main.ItemImage>
                <Main.DescriptionContainer>
                    <Main.Title>{title}</Main.Title>
                    <Main.ItemPrice>{value}</Main.ItemPrice>
                    <S.ReadMore to={`/product/${id}`}>{t('common.more')}</S.ReadMore>
                    {state.auth &&
                        <React.Fragment>
                        <Main.Input type='number'
                                min="1"
                                max={count}
                                value={chosenCount}
                                onChange={onChange} />
                        <Main.InStock>{t('common.inStock')} {count}</Main.InStock>
                        <Main.Button onClick={() => onBuy(data)}>{t('common.addToCard')}</Main.Button>
                        </React.Fragment>
                    }
                </Main.DescriptionContainer>
            </Main.SingleItem>
            {
            notificationMessage.messageVisibility &&
                <NotificationMsg message={notificationMessage.messageText} classname={notificationMessage.classname}/>
            }
        </React.Fragment>
    )
}