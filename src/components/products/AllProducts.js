import React, {useEffect, useState} from "react";
import i18n from 'i18next';
import Item from "./Item";
import data_en from "../../translations/en/products.json";
import data_arm from "../../translations/arm/products.json";
import data_ru from "../../translations/ru/products.json";
import * as S from "./AllProducts_styles";

export default function AllProducts() {
    const datas = {
        "en": data_en,
        "arm": data_arm,
        "ru": data_ru,
    }

    const [data, setData] = useState(i18n.language ? datas[i18n.language] : datas.en)

    useEffect(() => {
        i18n.on('languageChanged', (e) => {
            setData(datas[e])
        })
    },[datas])

    return (
        <React.Fragment>
            <S.ProductsContainer>
                {data.array.map((item, i) => {
                    if (item.count > 0) {
                        return <Item key={i}
                                     data={item}
                                     onBuy={item}/>
                    }
                })}
            </S.ProductsContainer>
        </React.Fragment>
    )
}