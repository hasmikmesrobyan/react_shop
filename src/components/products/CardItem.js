import React from "react";
import {useDispatch, useSelector} from 'react-redux';
import {useTranslation} from "react-i18next";
import * as Main from "../../css/Main_styles";
import * as S from "./Card_styles";

export default function CardItem({data, cardCount}) {

    const dispatch = useDispatch();

    const [t, i18n] = useTranslation('common');

    const userId = useSelector(state => state.auth.user.id)

    const onRemove = (itemId) => {
        dispatch({type: "REMOVE_ITEM", payload: {userId, itemId}})
        let userData = JSON.parse(localStorage.getItem('cardItems')),
            user = JSON.parse(localStorage.getItem('loggedUser'));
        if (userData[user.id]) {
            userData[user.id] = userData[user.id].filter(item => {
                return item.id !== itemId
            });
            localStorage.setItem('cardItems', JSON.stringify(userData))
            if (userData[user.id].length === 0) {
                localStorage.removeItem('cardItems');
                dispatch({type: "EMPTY_CARD", payload: {card: false}});
            }
        }
        ;
    }

    let {description, price, title, id, img} = data

    return (
        <Main.SingleItem className={'single_product'}>
            <S.LinkToSingle to={`/product/${id}`}>
                <Main.ItemImage style={{backgroundImage: `url(${img})`}}></Main.ItemImage>
            </S.LinkToSingle>
            <Main.DescriptionContainer  className={'single_product'}>
                <Main.Title>{title}</Main.Title>
                <Main.DescriptionText>{description}</Main.DescriptionText>
                <Main.ItemPrice>{t('singleItem.price')}: {price}</Main.ItemPrice>
                <Main.ItemPrice>{t('singleItem.quantity')}: {cardCount}</Main.ItemPrice>
                <Main.ItemPrice>{t('cardItem.amount')}: {(cardCount * parseInt(price.split('$')[0], 10))} $</Main.ItemPrice>
                <Main.Button className={'single_product'}
                             onClick={() => onRemove(id)}>
                    {t('cardItem.removeItem')}
                </Main.Button>
            </Main.DescriptionContainer>
        </Main.SingleItem>
    );
}