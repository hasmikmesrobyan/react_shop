import styled from "styled-components";

export const ProductsContainer = styled.div`
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
    
    @media (max-width: 600px) {
        width: 70%;
        margin: 0 auto;
    }
    @media (max-width: 480px) {
        width: 100%;
    }
`