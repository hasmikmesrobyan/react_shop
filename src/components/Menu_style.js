import styled from "styled-components";
import {Link} from "react-router-dom";

export const MenuList = styled.li`
    display: inline-block;
    margin-left: 10px;
`
export const MenuItem = styled(Link)`
    padding: 5px;
    text-transform: capitalize;
    color: #290034;
    font-size: 16px;
    position: relative;
    font-weight: 400;
    
    &:after{
        content: "";
        position: absolute;
        bottom: -3px;
        left: 0;
        right: 0;
        width: 0;
        transition: 0.3s all linear;
        background-color: #290034;
        height: 2px;
    }
    
    &:hover:after{
        width: 100%;
    }    
    
    &:active{
        color: #37334c;
    }
`