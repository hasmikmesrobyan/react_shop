import styled from "styled-components";

export const MessageContainer = styled.div`
    top: 54px;
    left: 0;
    bottom: 10px;
    right: 0;
    margin: 0 auto;
    position: fixed;
    z-index: 2;
`