import styled from "styled-components";
import {Link} from "react-router-dom";

export const Header = styled.header`
    width: 100%;
    padding: 10px 0;
    background-color: #e1dee2;
    position: fixed;
    box-shadow: 0 0 4px #aeb4be;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    z-index: 1;
    font-size: 16px
`
export const Nav = styled.nav`
    width: 100%;
    display: flex;
    align-items: flex-end;
    height: 60px;
    justify-content: space-between;
`
export const LogoContainer = styled(Link)`
    height: 90%;
    width: auto;
    display: inline-block;
    
    &:after{
        content: unset;
    }
`
export const LogoImage = styled.img`
    height: 100%;
    width: auto;
    object-fit: contain;
`
export const MenuContainer = styled.div`
    display: flex;
    align-items: center;
`
export const Button = styled.button`
    color: #290034;
    position: relative;
    border: none;
    background-color: transparent;
    outline: none;
    cursor: pointer;
    font-size: 16px;
    
    &.icon{
        display: inline-block;
        height: 30px;
        width: 30px;
        position: relative;
        border: none;
        background-color: transparent;
        outline: none;
    }
    
    &.user{
        margin-left: 20px;
        margin-right: 20px
    }
`
export const UserIcon = styled.img`
    object-fit: contain;
    height: 100%;
    width: 100%;
`
export const CardLink = styled(Link)`
    &.icon{
        display: inline-block;
        height: 30px;
        width: 30px;
        position: relative;
        border: none;
        background-color: transparent;
        outline: none;
    }
    
    &.card{
        margin-left: 40px;
        margin-right: 40px;  
    }
`
export const CardLinkImage = styled.img`
    object-fit: contain;
    height: 100%;
    width: 100%;
`
export const CardItemsCount = styled.span`
    position: absolute;
    top: -5px;
    left: 60%;
    margin: auto;
    color: #f8a406;
    font-size: 16px;
    text-transform: uppercase;
    height: min-content;
    width: min-content;
    display: inline-block;
    font-weight: 900;
    
    &.active{
        display: inline-block;
    }
    
     &.passive{
        display: none;
    }
`
export const UserName = styled.span`
    color: #290034;
    text-transform: capitalize;
    margin-right: 20px
`

export const LangBtn = styled.button`
    border:none;
    background-color: transparent;
    color: #290034;
    font-size: 16px;
    padding: 5px;
    position: relative;
    outline: none;
       
    &:active{
        border: none;
    }
    
    &:focus{
        border: none;
    }
`