import React, { useState, useEffect } from 'react';
import i18n from "i18next";
import Header from "../Header";
import * as Main from "../../css/Main_styles";

export default function Example() {
    const [count, setCount] = useState(0);

    const [value, setValue] = useState('');

   /* useEffect(() => {
        document.title = `You clicked ${count} times`;
    });

    i18n.on('languageChanged', (e) => {
        console.log(e)
    })
*//*
    useEffect(() => {
        setValue('ok')
    }, [i18n])
*/
    return (
        <React.Fragment>
            <Header/>
            <Main.Main>
                <Main.Wrapper>
                    <p>{value}</p>
                    <button onClick={() => setCount(count + 1)}>
                        Click me
                    </button>
                </Main.Wrapper>
            </Main.Main>
        </React.Fragment>
    );
}