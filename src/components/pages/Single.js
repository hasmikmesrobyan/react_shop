import React, {useEffect, useState} from "react";
import {useParams} from "react-router-dom";
import Header from '../Header';
import SingleProduct from "../products/SingleProduct";
import Title from "../Titles";
import * as Main from '../../css/Main_styles';
import data_en from "../../translations/en/products.json";
import data_arm from "../../translations/arm/products.json";
import data_ru from "../../translations/ru/products.json";
import i18n from "i18next";

export default function Single() {

    const params = useParams();

    const datas = {
        "en": data_en,
        "arm": data_arm,
        "ru": data_ru,
    }

    const [data, setData] = useState(datas.en)

  /*  useEffect(() => {
        i18n.on('languageChanged', (e) => {
            setData(datas[e])
        })
    },[])
*/
    const submit = event => {
        event.preventDefault()
    }

    const singleProduct = data.array.find((item, i) => item.id === parseInt(params.id, 10));

    return (
        <React.Fragment>
            <Header/>
            <Main.Main>
                <Main.Wrapper>
                    <Title title={singleProduct.title}/>
                    <SingleProduct submit={submit}
                                   item={singleProduct}/>
                </Main.Wrapper>
            </Main.Main>
        </React.Fragment>
    );
}