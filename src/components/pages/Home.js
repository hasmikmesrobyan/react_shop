import React from "react";
import Header from '../Header';
import Title from '../Titles';
import AllProducts from "../products/AllProducts";
import * as Main from '../../css/Main_styles';

export default function Home() {
    return (
        <React.Fragment>
            <Header/>
            <Main.Main>
                <Main.Wrapper>
                    <Title title={'products'}/>
                    <AllProducts/>
                </Main.Wrapper>
            </Main.Main>
        </React.Fragment>
    );
}