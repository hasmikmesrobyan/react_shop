import React, {useEffect, useState} from "react";
import {useSelector} from 'react-redux';
import i18n from "i18next";
import Header from "../Header";
import Title from "../Titles";
import CardItem from '../products/CardItem';
import * as Main from '../../css/Main_styles';
import * as S from "../products/Card_styles";
import data_en from "../../translations/en/products.json";
import data_arm from "../../translations/arm/products.json";
import data_ru from "../../translations/ru/products.json";

export default function Card() {

    const card = useSelector(state => state.card[state.auth.user.id]);

    /*const datas = {
        "en": data_en,
        "arm": data_arm,
        "ru": data_ru,
    }

    const [data, setData] = useState(i18n.language ? datas[i18n.language] : datas.en)

    useEffect(() => {
        i18n.on('languageChanged', (e) => {
            setData(datas[e])
        })
    }, [])*/

    let totalAmount = 0;
    if (card !== undefined) {
        card.filter((item) => {
            totalAmount += parseInt(item.price.split('$')[0], 10) * item.cardCount;
            return totalAmount;
        })
    }
    totalAmount += ' $';

    return (
        <React.Fragment>
            <Header/>
            <Main.Main>
                <Main.Wrapper>
                    <Title title={'Card'}/>
                    {card &&
                    <S.TotalAmount>Total amount is <S.TotalAmountValue>{totalAmount}</S.TotalAmountValue></S.TotalAmount>
                    }
                    <S.CardContainer>
                        {
                            Object.keys(card || []).length > 0 ?
                                Object.keys(card).map((item, i) => {
                                    return <CardItem key={i}
                                                     data={card[item]}
                                                     cardCount={card[item].cardCount}/>;
                                }) :
                                <p className={"error"}>There is no item in your card</p>
                        }
                    </S.CardContainer>
                </Main.Wrapper>
            </Main.Main>
        </React.Fragment>
    );
}