import Home from './components/pages/Home'
import Single from "./components/pages/Single.js";
import Example from "./components/pages/Example.js";

const publicRoutes = [
    {
        path: "/",
        component: Home
    },
    {
        path: "/product/:id",
        component: Single
    },
    {
        path: "/test",
        component: Example
    },
];

export default publicRoutes;