import React from 'react';
import { withRouter, BrowserRouter, Route } from "react-router-dom";
import ReactDOM from 'react-dom';
import {I18nextProvider} from "react-i18next";
import i18next from "i18next";
import './index.css';
import App from './App';
import arm_header from "./translations/arm/common.json";
import en_header from "./translations/en/common.json";
import ru_header from "./translations/ru/common.json";

i18next.init({
    whitelist: ['en', 'arm', 'ru'],
    fallbackLng: 'en',
    interpolation: { escapeValue: false },
    lng: 'en',
    resources: {
        en: {
            common: en_header
        },
        arm: {
            common: arm_header
        },
        ru: {
            common: ru_header
        },
    },
});

ReactDOM.render(
    <React.StrictMode>
        <BrowserRouter basename={"/"}>
            <I18nextProvider i18n={i18next}>
                <Route path="/" component={withRouter(App)} />
            </I18nextProvider>
        </BrowserRouter>
    </React.StrictMode>,
    document.getElementById('root')
);

