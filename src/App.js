import React from 'react';
import { combineReducers, createStore } from "redux"
import { Provider } from 'react-redux'
import './App.css';
import './css/custom.css';
import { authReducer } from './reducers/AuthReducer';
import { cardReducer } from './reducers/CardReducer';
import Main from "./components/Main"

const reducers = combineReducers({
    auth : authReducer,
    card: cardReducer
  });

const store = createStore(reducers,  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

function App() {
    
    return (
        <Provider store={store}>
           <Main/>
        </Provider>
    );
}

export default App;