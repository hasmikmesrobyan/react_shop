import styled from "styled-components";

export const Wrapper = styled.div`
    width: 1280px;
    padding: 0 15px;
    margin: 0 auto;
    
    @media (max-width: 1310px) {
        width: 1140px;
    }
    @media (max-width: 1140px) {
        width: 90%;
    }
    @media (max-width: 992px) {
        width: 100%;
    }
`
export const Main = styled.main`
    padding-top: 120px;
`
export const Title = styled.h3`
    font-size: 18px;
    color: #f8a406;
    text-transform: uppercase;
    padding-bottom: 10px;
    font-weight: 700;
    
    &.single_product{
        display: none;
    }
    
    @media (max-width: 600px) {
        padding-top: 20px;
        padding-bottom: 0;
        padding-right: 0;
    }
`
export const ItemImage = styled.div`
    height: 250px;
    width: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;    
    
    &.single_product{
        width: 30%;
        
        @media (max-width: 992px) {
             width: 40%;
        }
    }
    
    @media (max-width: 768px) {
         height: 200px;
    }
    
    @media (max-width: 600px) {
        width: 100%;
        border-radius: unset;
    }
`
export const DescriptionContainer = styled.div`
    width: 100%;
    padding: 20px 10px;
    text-align: center;
    
    &.single_product{
        padding-bottom: 10px;
        padding-top: 10px;
        text-align: left;
    }
    
    @media (max-width: 600px) {
        width: 100%;
        display: flex;
        flex-direction: column;
        align-items: center;
        padding: 0 10px;
    }
    
`
export const DescriptionText = styled.p`
    font-size: 16px;
    margin-bottom: 10px;
    color: #290034;
    
    @media (max-width: 600px) {
        text-align: center;
        margin-top: 20px;
    }
`
export const Button = styled.button`
    outline: none;
    border-radius: 10px;
    border: 1px solid #aeb4be;
    background-color: white;
    color: #290034;
    font-size: 14px;
    text-transform: uppercase;
    height: 40px;
    min-width: 150px;
    transition: 0.3s all linear;
    display: block;
    padding-left: 30px;
    padding-right: 30px;
    font-weight: 700;
    width: max-content;
    margin-top: 20px;
    margin: 20px auto 0;
    cursor: pointer;
    
    &:hover{
        border: 1px solid #3f4246;
        box-shadow: 0 0 3px 1px #aeb4be;
    }
    
    &.single_product{
        margin-left: 0;
    }
`
export const Input = styled.input`
    outline: none;
    border-radius: 10px;
    border: 1px solid #aeb4be;
    background-color: white;
    color: #290034;
    font-size: 14px;
    height: 40px;
    width: 150px;
    display: block;
    padding-left: 10px;
    padding-right: 10px;
    margin: 20px auto 5px;
    cursor: pointer;
        
    &[type="number"]{
        text-align: center;
        
        &.single_product{
            margin-left: 0;
            margin-top: 10px;
        }
    }
    
`
export const InStock = styled.span`
    color: #f8a406;
    font-size: 14px;
`
export const ItemPrice = styled.p`
    margin-bottom: 10px;
    font-size: 1rem;
    color: #290034;
    line-height: 18px;
    
    @media (max-width: 600px) {
        margin: 0;
    }
`
export const SingleItem = styled.div`
    width: calc(100% / 3 - 40px);
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 40px;
    border-radius: 10px;
    overflow: hidden;
    background-color: white;
    box-shadow: 0px 2px 6px 1px #e1dee2;
    position: relative;
    
    &.single_product{
        display: flex;
        flex-wrap: nowrap;
        width: 100%;
        flex-direction: row;
        align-items: flex-start;
        
        @media (max-width: 600px) {
             border-radius: 10px;
            overflow: hidden;
            background-color: white;
            padding-bottom: 20px;
            box-shadow: 0px 2px 6px 1px #e1dee2;
            flex-direction: column;
        }
    }
    
    @media (max-width: 1280px) {
        width: calc(100% / 3 - 40px);
    }
    @media (max-width: 768px) {
        width: calc(100% / 2 - 20px);
    }
    @media (max-width: 600px) {
        width: 100%;
        flex-direction: column;
        align-items: center;
        padding-bottom: 10px;
    }
`
export const Label = styled.label`
    display: block;
    margin-top: 20px;
    color: #290034;
    
    @media (max-width: 600px) {
       text-align: center;
    }
`