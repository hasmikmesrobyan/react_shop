import Card from "./components/pages/Card.js";
import Home from './components/pages/Home'
import Single from "./components/pages/Single.js";

const privateRoutes = [
    {
        path: "/",
        component: Home
    },
    {
        path: "/card",
        component: Card
    },
    {
        path: "/product/:id",
        component: Single
    },
    
];

export default privateRoutes;